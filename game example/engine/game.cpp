#include "game.h"

Game::Game(const char* window_name, int game_width, int game_height)
{
	graphics = new Graphics(window_name, game_width, game_height);
	sceneHandler = new SceneHandler(SCENE_FIRST_MAP);
	fps = new Fps();
}

Game::~Game()
{}

bool Game::init()
{
	//Window and Media stuff here
	if(!graphics->initializeSDL()) return false;
	if(!graphics->createWindow()) return false;
	if(!AudioHandler::initializeSDLAudio()) return false;
	return true;
}

bool Game::run()
{
	bool quit = false;

	SDL_Event e;

	fps->resetTimeStep();

	while (quit != true)
	{
		Scene* sceneToCompute = sceneHandler->getCurrentScene();
		//Event handling here
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}

			for (int i = 0; i < sceneToCompute->getSize(); i++)
			{
				sceneToCompute->getObject(i)->handleEvent(e);
			}
		}

		printf("%f\n", fps->countFps());

		//Set current time
		double time = fps->getTimeStep();

		//It restarts time for next frame.
		fps->resetTimeStep();


		//Clear render
		graphics->renderClear();

		
		//Move the objects
		for (int i = 0; i < sceneToCompute->getSize(); i++)
		{
			sceneToCompute->getObject(i)->updatePosition(time);
		}

		//Collisions handling
		for (int i = 0; i < sceneToCompute->getSize(); i++)
		{
			for (int j = 0; j < sceneToCompute->getSize(); j++)
			{
				//Skip checking yourself
				if (i == j) continue;
				sceneToCompute->getObject(i)->handleCollision(sceneToCompute->getObject(j), time);
			}
		}

		//Handle custom update
		for (int i = 0; i < sceneToCompute->getSize(); i++)
		{
			sceneToCompute->getObject(i)->update();
		}

		//Render current frame
		for (int i = 0; i < sceneToCompute->getSize(); i++)
		{
			sceneToCompute->getObject(i)->updateAnimation(time);
			if(sceneToCompute->getObject(i)->isVisible())
				sceneToCompute->getObject(i)->render();
		}

		//Erase objects to delete
		sceneToCompute->deleteObjectsToDelete();

		//Update screen
		graphics->renderUpdate();

		//Increment application's frames counter.
		fps->incrementFramesCounter();
	}
	return true;
}

void Game::cleanup()
{
	//Cleaning up allocated space
	graphics->cleanup();
	delete sceneHandler;
	delete fps;
	TTF_Quit();
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}

Graphics *Game::getGraphics()
{
	return graphics;
}

SceneHandler *Game::getSceneHandler()
{
	return sceneHandler;
}
