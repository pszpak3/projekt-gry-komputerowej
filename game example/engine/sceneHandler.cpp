#include "sceneHandler.h"

SceneHandler::SceneHandler(int currentScene)
{
	scene = new Scene[SCENE_TOTAL];
	m_currentScene = currentScene;
}

SceneHandler::~SceneHandler()
{
	delete [] scene;
}

void SceneHandler::changeScene(SceneEnum scene)
{
	assert(scene < SCENE_TOTAL);
	m_currentScene = scene;
}

Scene * SceneHandler::getScene(SceneEnum nr)
{
	return &scene[nr];
}

Scene * SceneHandler::getCurrentScene()
{
	return &scene[m_currentScene];
}

Scene* SceneHandler::getAllScenes()
{
	return scene;
}
