#include "animation.h"
#include "textureSpriteSheet.h"
#include "texture.h"

Animation::Animation(TextureSpriteSheet tss, double seconds_to_change_clip, int state_name, bool loop)
{
	m_tss = tss;
	m_current_frame = 0;
	m_seconds_to_change_clip = seconds_to_change_clip;
	m_animation_passed_time = 0;
	m_state_name = state_name;
	m_loop = loop;

	for (int rowInPixels = 0; rowInPixels <= m_tss.getClipHeight() * m_tss.getRowCount() - m_tss.getClipHeight(); rowInPixels += m_tss.getClipHeight())
	{
		for (int colInPixels = 0; colInPixels <= m_tss.getClipWidth() * m_tss.getColumnCount() - m_tss.getClipWidth(); colInPixels += m_tss.getClipWidth())
		{
			m_frames.push_back(new Frame(colInPixels, rowInPixels));
		}
	}
}
Animation::~Animation(){}

void Animation::nextFrame()
{
	if (m_current_frame >= m_frames.size()-1)
	{
		if (m_loop)
			m_current_frame = 0;
		else
			m_current_frame = m_frames.size() - 1;
	}
	else
	{
		m_current_frame++;
	}
}

void Animation::reset()
{
	m_current_frame = 0;
}

bool Animation::isLooping()
{
	return m_loop;
}

void Animation::setLooping(bool loop)
{
	m_loop = loop;
}

Texture Animation::getCurrentTexture(double time)
{
	m_animation_passed_time += time;

	if (m_animation_passed_time >= m_seconds_to_change_clip) 
	{
		m_animation_passed_time = 0;
		nextFrame();
	}
	Texture ret_texture(m_frames.at(m_current_frame)->m_col, m_frames.at(m_current_frame)->m_row, m_tss.m_clip_width, m_tss.m_clip_height, m_tss.getSpriteSheetTexture());
	return ret_texture;
}

Frame * Animation::getCurrentFrame()
{
	return m_frames.at(m_current_frame);
}

double Animation::getSecondsToChangeClip()
{
	return m_seconds_to_change_clip;
}

int Animation::getStateName()
{
	return m_state_name;
}

List<Frame*>* Animation::getFrameList()
{
	return &m_frames;
}

TextureSpriteSheet* Animation::getTextureSpriteSheet()
{
	return &m_tss;
}


