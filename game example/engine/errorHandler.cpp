#include "errorHandler.h"

void ErrorHandler::errorMessageBox(std::string errorTitle, std::string errorMessage, SDL_Window *window)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, errorTitle.c_str(), errorMessage.c_str(), window);
}

void ErrorHandler::errorMessageBox(std::string errorTitle, char* errorMessage, SDL_Window *window)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, errorTitle.c_str(), errorMessage, window);
}