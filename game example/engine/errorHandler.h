#ifndef ERRORHANDLER_H_INCLUDED
#define ERRORHANDLER_H_INCLUDED

#include <string>
#include <SDL.h>

class ErrorHandler
{
public:
	ErrorHandler()							= delete;
	ErrorHandler(const ErrorHandler &other) = delete;
	~ErrorHandler()							= delete;
	static void errorMessageBox(std::string errorTitle, std::string errorMessage, SDL_Window *window = NULL);
	static void errorMessageBox(std::string errorTitle, char *errorMessage, SDL_Window *window = NULL);
};

#endif // ERRORHANDLER_H_INCLUDED
