#include "textureSpriteSheet.h"

TextureSpriteSheet::TextureSpriteSheet()
{
}

TextureSpriteSheet::TextureSpriteSheet(int row_count, int column_count, int clip_width, int clip_height)
{
	m_row_count = row_count;
	m_column_count = column_count;
	m_clip_width = clip_width;
	m_clip_height = clip_height;

}



int TextureSpriteSheet::getAmountOfClips()
{
	return m_row_count * m_column_count;
}

int TextureSpriteSheet::getRowCount()
{
	return m_row_count;
}

int TextureSpriteSheet::getColumnCount()
{
	return m_column_count;
}

int TextureSpriteSheet::getClipHeight()
{
	return m_clip_height;
}

int TextureSpriteSheet::getClipWidth()
{
	return m_clip_width;
}

TextureSpriteSheet::~TextureSpriteSheet() {}

SDL_Texture* TextureSpriteSheet::getSpriteSheetTexture()
{
	return m_sprite_sheet_texture;
}

void TextureSpriteSheet::setSpriteSheetTexture(SDL_Texture* sprite_sheet_stexutre)
{
	m_sprite_sheet_texture = sprite_sheet_stexutre;
}


