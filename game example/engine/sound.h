#ifndef _SOUNDEFFECT_H_
#define	_SOUNDEFFECT_H_
#include <SDL_mixer.h>

struct Sound
{
	int id;
	int channel;
	Mix_Chunk* sound;
	Sound(int id, int channel, Mix_Chunk* sound);
};

#endif