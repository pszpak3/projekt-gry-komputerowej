#ifndef _OBJECT_H_
#define	_OBJECT_H_
#include <string>
#include "graphics.h"
#include "animation.h"
#include "animationHandler.h"
#include "texture.h"
#include "enums.h"
#include "hitBox.h"
#include "sceneHandler.h"
class SceneHandler;
class Scene;

class Object
{
private:
	Graphics* m_graphics;
	SceneHandler* m_sceneHandler;
	AnimationHandler* m_animation_handler;

	List<unsigned int> m_types;
	static inline int m_objects_id_setter = 0;
	int m_id;

	bool m_visibility;

	Texture m_current_texture;

	HitBox maxRangeHitbox;

protected:
	//Position
	double m_pos_x;
	double m_pos_y;

	//Acceleration in pixels per second squared.
	double m_acceleration_y;

	//Velocity in pixels per second.
	double m_current_vel_x;
	double m_current_vel_y;
	double m_walking_vel;

public:
	Object(Graphics *graphics, SceneHandler *sceneHandler);
	~Object();

	Graphics* getGraphics();
	SceneHandler* getSceneHandler();

	//-------Position---------
	void setPosX(double pos_x);
	void setPosY(double pos_y);
	double getPosX();
	double getPosY();

	//-------Animations-------
	AnimationHandler* getAnimationHandler();
	void addAnimation(Animation* animation);
	void changeAnimation(int state_name, double angle = 0.0, SDL_RendererFlip flip = SDL_FLIP_NONE);
	Animation* getCurrentAnimation();

	//--------Physics---------
	double getWalkingVel();
	double getVelX();
	double getVelY();
	void setVelX(double vel_x);
	void setVelY(double vel_y);
	void setWalkingVel(double walking_vel);
	double getAccelerationY();
	void setAccelerationY(double new_acceleration);
	Uint8 getVelocityDirection();

	//-------Visibility-------
	bool isVisible();
	void setVisibility(bool new_visibility);

	//--------Deletion--------
	//Returns true if deleted this object from the given scene.
	bool deleteFromScene(SceneEnum scene);
	bool deleteFromCurrentScene();
	//Permanently deletes object from all scenes.
	//The function deletes the object from memory, not only from scene.
	void deletePermanently();

	//---Collision handling----
	//Sets hitboxes for all frames the same as the frame parameters.
	void setHitboxForAllFrames();
	//Returns super hitbox covering all hitboxes.
	HitBox getMaxRangeHitbox();
	void setMaxRangeHitbox();
	//Checks if object touches other object from bottom side.
	//So if this object is 1 pixel above the other object, it returns true for touchesFromBelow(other); (this object touches other from below).
	bool touchesFromBelow(Object* other);
	bool touchesLowerBound();
	void stop(Object* object_to_be_stopped_from, double time_step);

	//--------Textures---------
	void setCurrentTexture(Texture texture);
	void setTexture(std::string path, int state_name = 0);
	void setTextTexture(std::string text, std::string font_path, int font_size = 12, SDL_Color font_color = { 0, 0 ,0 }, int state_name = 0);
	
	//------Type handling------
	//Checks if an object is of the type passed in argument.
	bool isOfType(unsigned int type_id);
	void addType(unsigned int type_id);
	void removeType(unsigned int type_id);
	int getId();

	//-------Overridable-------
	virtual void update();
	virtual void onKeyboardInputUp(SDL_Keycode keyCode);
	virtual void onKeyboardInputDown(SDL_Keycode keyCode);
	virtual void onMouseHover();
	virtual void onMouseClick();
	virtual void onCollision(Object* other, double timestep);

private:
	friend class Game;
	//-----Engine functions----
	//Handling inputs
	void handleEvent(SDL_Event& e);
	void handleMouseHover(SDL_Event &e);
	bool cursorAtObject(SDL_Event& e);

	//Collision handling
	void handleCollision(Object* other, double timestep);
	bool cursorCollides(int cursor_x, int cursor_y, int x, int y, int w, int h);
	bool collides(int x1, int y1, int h1, int w1,
				  int x2, int y2, int h2, int w2);
	bool collidesWith(Object* other);

	//Update functions
	void updateAnimation(double time);
	void updatePosition(double timeStep);
	void render();

	//Helper functions
	void setSingleFrameForAnimation(Texture texture, int state_name);
	static unsigned int getNewId();
	bool deleteFromScene(Scene* scene, bool permanently = false);
};

#endif //_OBJECT_H_