#ifndef _OBJECT_DATA_H_
#define _OBJECT_DATA_H_
#include "object.h"

class Object;

struct ObjectData
{
	Object* object = NULL;
	bool toDelete = false;
	bool deletePermanent = false;
};

#endif //_OBJECT_DATA_H_