#ifndef SCENE_H
#define SCENE_H
#include "constants.h"
#include "object.h"
#include "objectData.h"
#include "errorHandler.h"

class Object;

class Scene
{
	List<ObjectData> objects;

public:
	Scene();
	~Scene();

	void addObject(Object* obj);
	void addObject(Object* obj, int x_pos, int y_pos);
	Object* getObject(int i);
	List<ObjectData>& getObjectDatas();
	void deleteObjectsToDelete();
	int getSize();
};

#endif SCENE_H //SCENE_H