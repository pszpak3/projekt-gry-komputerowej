#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <vector>

// We'll use
// List<int> list;
// instead of
// std::vector<int> list;
template <class T>
using List = std::vector<T>;

enum ScalingQualityEnum
{
	NEAREST,
	LINEAR,
	ANISOTROPIC
};

#endif // CONSTANTS_H