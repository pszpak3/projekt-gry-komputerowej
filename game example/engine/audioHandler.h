#ifndef _SOUNDHANDLER_H_
#define	_SOUNDHANDLER_H_

#include <SDL_mixer.h>
#include "constants.h"
#include "music.h"
#include "sound.h"
#include <string>

class AudioHandler
{
	static inline List<Music> m_music_list;
	static inline List<Sound> m_sound_list;
	static void deleteChannelFromSound(int channel);

public:
	AudioHandler() = delete;
	AudioHandler(const AudioHandler& other) = delete;
	~AudioHandler() = delete;

	static bool initializeSDLAudio(int frequency = 44100, Uint16 format = MIX_DEFAULT_FORMAT, int channels = 2, int soundSize = 2048);
	static bool addMusic(int id, std::string path);
	static bool addSound(int id, std::string path);
	static void playMusic(int id, int timesRepeated = -1);
	static void stopCurrentMusic();
	static void pauseCurrentMusic();
	static void unpauseCurrentMusic();
	static void playSound(int id, bool wait_for_channel_to_finish = false, int timesRepeated = 0);
};

#endif