#include "fps.h"

Fps::Fps() 
{
	m_fps_timer.start();
	m_fim_timer.start();
	m_counted_frames = 0;
	m_fps = 0.0;
}

Fps::~Fps()
{}

double Fps::countFps()
{
	if (m_fps_timer.getTimeInSeconds() != 0.0)
	{
		m_fps = m_counted_frames / (m_fps_timer.getTimeInSeconds());
		
		if (m_fps > 2000000.0)
		{
			m_fps = 0.0;
		}
	}
	return m_fps;
}

void Fps::incrementFramesCounter()
{
	m_counted_frames++;
}

double Fps::getTimeStep()
{
	double time = m_fim_timer.getTimeInSeconds();
	return time;
}

void Fps::resetTimeStep()
{
	m_fim_timer.start();
}

