#include "scene.h"

Scene::Scene()
{}

Scene::~Scene()
{}

void Scene::addObject(Object* obj)
{
	obj->setMaxRangeHitbox();
	objects.push_back({ obj, false });
}

void Scene::addObject(Object* obj, int x_pos, int y_pos)
{
	assert(obj != NULL || x_pos >= 0 || y_pos >= 0);
	obj->setPosX(x_pos);
	obj->setPosY(y_pos);
	addObject(obj);
}

Object* Scene::getObject(int i)
{
	if (i >= objects.size() || i < 0)
	{
		ErrorHandler::errorMessageBox(std::to_string(i) + "Index you gave is out of bounds!", SDL_GetError());
		return NULL;
	}

	return objects[i].object;
}

List<ObjectData>& Scene::getObjectDatas()
{
	return objects;
}

void Scene::deleteObjectsToDelete()
{
	for (int i = 0; i < objects.size(); i++)
	{
		if (objects[i].toDelete)
		{
			if (objects[i].object != NULL && objects[i].deletePermanent)
				delete objects[i].object;
			objects.erase(objects.begin() + i);
			i--;
		}
	}
}

int Scene::getSize()
{
	return objects.size();
}
