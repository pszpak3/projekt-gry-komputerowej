//class for storing sprite sheets with information
//about base width and height of a each frame

#ifndef TEXTURE_SPRITESHEET_H
#define TEXTURE_SPRITESHEET_H
#include <SDL.h>
struct TextureSpriteSheet
{
	int m_row_count;
	int m_column_count;
	int m_clip_width;
	int m_clip_height;
	SDL_Texture* m_sprite_sheet_texture;
public:
	TextureSpriteSheet();
	TextureSpriteSheet(int row_count, int column_count, int clip_width, int clip_height);
	~TextureSpriteSheet();
	int getRowCount();
	int getColumnCount();
	int getClipWidth();
	int getClipHeight();

	int getAmountOfClips();
	
	void setSpriteSheetTexture(SDL_Texture* sprite_sheet_stexutre);
	SDL_Texture* getSpriteSheetTexture();
	
};
#endif //TEXTURE_SPRITESHEET_H