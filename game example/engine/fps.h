#ifndef FPS_H_INCLUDED
#define FPS_H_INCLUDED
#include "timer.h"

class Fps
{
private:
	//Timer for counting FPS.
	Timer m_fps_timer;
	int m_counted_frames;
	double m_fps;

	//Timer for frame independent movement.
	Timer m_fim_timer;
public:
	Fps();
	~Fps();
	double countFps();
	void incrementFramesCounter();

	//Returns time in seconds counted by fim_timer.
	double getTimeStep();

	//It is used to restart fim_timer.
	void resetTimeStep();
};

#endif