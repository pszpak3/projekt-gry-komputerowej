#ifndef SCENE_HANDLER_H
#define SCENE_HANDLER_H
#include "enums.h"
#include "scene.h"
#include <cassert>
class Scene;
//This object should be initialized at the beggining and only then,
//because it gets info from Enum.h about total number of predicted scenes for the array to initialize.
class SceneHandler
{
	Scene* scene;
	int m_currentScene;
public:
	SceneHandler(int currentScene);
	~SceneHandler();

	//Usuwamy konstruktor kopiuj�cy - gdyby�my tego nie napisali, to kompilator zupe�nie niepotrzebnie by go stworzy�, a nie jest potrzebny.
	SceneHandler(const SceneHandler& other) = delete;

public:
	Scene *getScene(SceneEnum nr);
	Scene *getCurrentScene();
	Scene* getAllScenes();
	void changeScene(SceneEnum scene);

};

#endif //SCENE_HANDLER_H