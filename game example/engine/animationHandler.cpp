#include "animationHandler.h"
#include "enums.h"
#include "textureSpriteSheet.h"
#include "texture.h"

AnimationHandler::AnimationHandler()
{
	m_flip = SDL_FLIP_NONE;
	m_angle = 0.0;
	m_current_animation_state_name = 0;
}
AnimationHandler::~AnimationHandler() {}

void AnimationHandler::addAnimation(Animation *animation)
{
	assert(animation != NULL);
	if (m_animations.empty())
	{
		setCurrentAnimation(animation->getStateName());
	}
	m_animations.push_back(animation);
}

void AnimationHandler::setCurrentAnimation(int state_name)
{
	m_current_animation_state_name = state_name;
}

Animation * AnimationHandler::getCurrentAnimation()
{
	for (auto animation : m_animations)
	{
		if (animation->getStateName() == m_current_animation_state_name)
		{
			return animation;
		}
	}
	return NULL;
}

Animation* AnimationHandler::getAnimation(int state_name)
{
	for (auto animation : m_animations)
	{
		if (animation->getStateName() == state_name)
		{
			return animation;
		}
	}
	return NULL;
}

void AnimationHandler::setFlip(SDL_RendererFlip flip)
{
	m_flip = flip;
}

void AnimationHandler::setAngle(double angle)
{
	m_angle = angle;
}

SDL_RendererFlip AnimationHandler::getFlip()
{
	return m_flip;
}

double AnimationHandler::getAngle()
{
	return m_angle;
}

void AnimationHandler::setHitboxForAllFrames()
{
	for (int i = 0; i < m_animations.size(); i++)
	{
		for (int j = 0; j < m_animations.at(i)->getFrameList()->size(); j++)
		{
			TextureSpriteSheet* tss = m_animations.at(i)->getTextureSpriteSheet();
			m_animations.at(i)->getFrameList()->at(j)->getCollisionHitboxList()->push_back(new HitBox(0, 0, tss->getClipWidth(), tss->getClipHeight()));
		}
	}

}