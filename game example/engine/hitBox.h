#ifndef _HITBOX_H_
#define _HITBOX_H_

class HitBox
{
private:
	int m_x, m_y;
	int m_w, m_h;
public:
	HitBox();
	HitBox(int x, int y, int w, int h);
	void set(int x, int y, int w, int h);
	int getX();
	int getY();
	int getW();
	int getH();
};

#endif //_HITBOX_H_