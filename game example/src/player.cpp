#include "player.h"

Player::Player(Graphics* graphics, SceneHandler* sceneHandler) : Object(graphics, sceneHandler)
{
	m_acceleration_y = 2000.0;
}

//If a key was pressed
void Player::onKeyboardInputDown(SDL_Keycode keyCode)
{
	//Adjust the velocity
	switch (keyCode)
	{
	case SDLK_UP:
		if(!died && !won)
			jump(850.0);
		break;
	case SDLK_LEFT:
		if (!died && !won)
			m_current_vel_x -= m_walking_vel;
		break;
	case SDLK_RIGHT:
		if (!died && !won)
			m_current_vel_x += m_walking_vel;
		break;
	case SDLK_1: getGraphics()->setResolution(320, 240); break;
	case SDLK_2: getGraphics()->setResolution(640, 480); break;
	case SDLK_3: getGraphics()->setResolution(1024, 768); break;
	case SDLK_4: getGraphics()->setResolution(1366, 768); break;
	case SDLK_5: getGraphics()->setResolution(1920, 1080); break;
	case SDLK_6: getGraphics()->setScalingQuality(NEAREST); break;
	case SDLK_7: getGraphics()->setScalingQuality(LINEAR); break;
	case SDLK_8: getGraphics()->setScalingQuality(ANISOTROPIC); break;
	case SDLK_9: getGraphics()->setLetterBoxing(true); break;
	case SDLK_0: getGraphics()->setLetterBoxing(false); break;
	case SDLK_ESCAPE:
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
	}

}

//If a key was released
void Player::onKeyboardInputUp(SDL_Keycode keyCode)
{
	//Adjust the velocity
	switch (keyCode)
	{
	case SDLK_LEFT:
		if (!died && !won)
			m_current_vel_x += m_walking_vel;
		break;
	case SDLK_RIGHT:
		if (!died && !won)
			m_current_vel_x -= m_walking_vel;
		break;
	}
}

void Player::jump(double jump_vel)
{
	if (canJump)
	{
		AudioHandler::playSound(SOUND_JUMP);
		m_current_vel_y -= jump_vel;
		canJump = false;
	}
}

void Player::onCollision(Object* other, double timestep)
{
	//who not to block
	if(!other->isOfType(TYPE_SIGN) && !other->isOfType(TYPE_KEY) && !other->isOfType(TYPE_TEXTBOARD) && !other->isOfType(TYPE_LANTERN) && !other->isOfType(TYPE_CHEST))
		stop(other, timestep);

	//sign behaviour handling
	if (other->isOfType(TYPE_SIGN_TO_SECOND_MAP))
	{
		setPosX(100.0);
		setPosY(792.0);
		getSceneHandler()->changeScene(SCENE_SECOND_MAP);
	}
	else if (other->isOfType(TYPE_SIGN_TO_FIRST_MAP))
	{
		setPosX(1800.0);
		setPosY(692.0);
		getSceneHandler()->changeScene(SCENE_FIRST_MAP);
	}

	if (other->isOfType(TYPE_GROUND) && getVelX() != 0)
	{
		AudioHandler::playSound(SOUND_FOOTSTEP, true);
	}

	if (other->isOfType(TYPE_KEY))
	{
		AudioHandler::playSound(SOUND_PICK_UP, true);
		hasKey = true;
		//other->deletePermanently();
		other->deleteFromCurrentScene();
		//other->deleteFromScene(SCENE_SECOND_MAP);
		//other->setVisibility(false);
	}

	if (other->isOfType(TYPE_SPIKE))
	{
		die();
	}

	if (other->isOfType(TYPE_CHEST) && hasKey && !collectedItemFromChest)
	{
		AudioHandler::playSound(SOUND_CHEST_OPEN, true);
		other->changeAnimation(STATE_CHEST_OPENED);
		collectedItemFromChest = true;
		win();
	}

	//jump handling
	if (touchesFromBelow(other))
	{
		canJump = true;
	}
}

void Player::update()
{
	if (getVelY() != 0.0)
	{
		canJump = false;
	}
	if (touchesLowerBound())
	{
		die();
	}
	if (getVelX() < 0 && getCurrentAnimation()->getStateName() != STATE_WALKING && getVelY() == 0.0)
	{
		changeAnimation(STATE_WALKING, 0.0, SDL_FLIP_HORIZONTAL);
	}
	else if (getVelX() > 0 && getCurrentAnimation()->getStateName() != STATE_WALKING && getVelY() == 0.0)
	{
		changeAnimation(STATE_WALKING, 0.0, SDL_FLIP_NONE);
	}
	else if (getVelY() < 0 && getCurrentAnimation()->getStateName() != STATE_JUMPING)
	{
		changeAnimation(STATE_JUMPING, 0.0, getAnimationHandler()->getFlip());
	}
	else if (getVelY() > 0 && getCurrentAnimation()->getStateName() != STATE_FALLING)
	{
		changeAnimation(STATE_FALLING, 0.0, getAnimationHandler()->getFlip());
	}
	else if (died)
	{
		changeAnimation(STATE_DYING, 0.0, getAnimationHandler()->getFlip());
	}
	else if (won)
	{
		changeAnimation(STATE_STANDING, 0.0, getAnimationHandler()->getFlip());
	}
	else if (getCurrentAnimation()->getStateName() != STATE_STANDING && getCurrentAnimation()->getStateName() != STATE_DYING && getVelX() == 0 && getVelY() == 0)
	{
		changeAnimation(STATE_STANDING, 0.0, getAnimationHandler()->getFlip());
	}
	//win handling
	if (win_screen->getPosY() > getGraphics()->getLowerBound())
	{
		win_screen->deleteFromScene(SCENE_FIRST_MAP);
		win_screen->deleteFromScene(SCENE_SECOND_MAP);
	}
}

void Player::onMouseClick()
{
	getWalkingVel() == 300.0 ? setWalkingVel(600.0) : setWalkingVel(300.0);
}

void Player::setLooseScreen(RestartScreen* obj)
{
	loose_screen = obj;
}

void Player::setWinScreen(RestartScreen* obj)
{
	win_screen = obj;
}

void Player::revieve()
{
	setPosX(670);
	setPosY(692);
	getAnimationHandler()->getAnimation(STATE_DYING)->reset();
	changeAnimation(STATE_STANDING, 0.0, SDL_FLIP_NONE);
	died = false;
	won = false;
}

void Player::setHasKey(bool _hasKey)
{
	hasKey = _hasKey;
}

void Player::setCollectedItemFromChest(bool _collectedItemFromChest)
{
	collectedItemFromChest = _collectedItemFromChest;
}

bool Player::getHasKey()
{
	return hasKey;
}

bool Player::getCollectedItemFromChest()
{
	return collectedItemFromChest;
}

void Player::die()
{
	if (!died)
	{
		AudioHandler::playSound(SOUND_DYING, true);
		died = true;
		setVelX(0.0);
		loose_screen->show();
	}
}

void Player::win()
{
	if (!won)
	{
		won = true;
		setVelX(0.0);
		win_screen->show();
	}
}
