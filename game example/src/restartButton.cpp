#include "restartButton.h"

RestartButton::RestartButton(Graphics* graphics, SceneHandler* sceneHandler, Player* _player, RestartScreen* _restartScreen) : Object(graphics, sceneHandler)
{
	player = _player;
	restartScreen = _restartScreen;
}

void RestartButton::onMouseClick()
{
	restartGame();
	restartScreen->hide();
}

void RestartButton::revievePlayer()
{
	getSceneHandler()->changeScene(SCENE_FIRST_MAP);
	player->revieve();
}

void RestartButton::restartGame()
{
	//restart Player
	revievePlayer();

	//restore Key
	if (player->getHasKey())
	{
		player->setHasKey(false);
		getSceneHandler()->getScene(SCENE_SECOND_MAP)->addObject(restartScreen->getKey());
	}

	//restore Chest
	if (player->getCollectedItemFromChest())
	{
		player->setCollectedItemFromChest(false);
		restartScreen->getChest()->changeAnimation(STATE_CHEST_CLOSED);
	}
}
