#ifndef _WALL_H_
#define _WALL_H_
#include "../engine/object.h"
#include "../engine/graphics.h"

class Wall : public Object
{
public:
	Wall(Graphics* graphics, SceneHandler* sceneHandler);
};

#endif //_WALL_H_