#include "lantern.h"

Lantern::Lantern(Graphics* graphics, SceneHandler* sceneHandler) : Object(graphics, sceneHandler)
{}

void Lantern::onCollision(Object* other, double timestep)
{
	if (other->isOfType(TYPE_PLAYER))
	{
		changeAnimation(STATE_LANTERN_ON);
		m_visibility_timer.start();
	}
}

void Lantern::update()
{
	if (m_visibility_timer.getTimeInSeconds() > 5.0)
	{
		changeAnimation(STATE_LANTERN_OFF);
		m_visibility_timer.stop();
	}
}

