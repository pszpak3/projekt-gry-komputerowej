#ifndef _LANTERN_H_
#define _LANTERN_H_
#include "../engine/object.h"
#include "../engine/graphics.h"
#include "../engine/timer.h"

class Lantern : public Object
{
private:
	Timer m_visibility_timer;
public:
	Lantern(Graphics* graphics, SceneHandler* sceneHandler);
	void onCollision(Object* other, double timestep) override;
	void update() override;
};
#endif //_LANTERN_H_