#ifndef _BACKGROUND_H_
#define _BACKGROUND_H_
#include "../engine/object.h"
#include "../engine/graphics.h"

class Background : public Object
{
public:
	Background(Graphics* graphics, SceneHandler* sceneHandler);
};

#endif //_BACKGROUND_H_