#ifndef _RESTART_BUTTON_H_
#define _RESTART_BUTTON_H_
#include "../engine/object.h"
#include "../engine/graphics.h"
#include "player.h"
#include "restartScreen.h"

class Player;
class RestartScreen;

class RestartButton : public Object
{
	Player* player = NULL;
	RestartScreen* restartScreen = NULL;
public:
	RestartButton(Graphics* graphics, SceneHandler* sceneHandler, Player* player_to_revieve, RestartScreen* restart_screen);
	void onMouseClick() override;
	void revievePlayer();
	void restartGame();
};

#endif //_RESTART_BUTTON_H_