#ifndef _PLAYER_H_
#define _PLAYER_H_
#include "../engine/object.h"
#include "../engine/graphics.h"
#include "../engine/audioHandler.h"
#include "restartScreen.h"

class RestartScreen;
class Background;

class Player : public Object
{
	bool canJump = false;
	bool hasKey = false;
	bool collectedItemFromChest = false;
	bool died = false;
	bool won = false;
	RestartScreen* loose_screen = NULL;
	RestartScreen* win_screen = NULL;
public:
	Player(Graphics* graphics, SceneHandler* sceneHandler);
	void setLooseScreen(RestartScreen* obj);
	void setWinScreen(RestartScreen* obj);
	void revieve();
	void setHasKey(bool hasKey);
	void setCollectedItemFromChest(bool collectedItemFromChest);
	bool getHasKey();
	bool getCollectedItemFromChest();
	void die();
	void win();
	void jump(double jump_vel);

	//Overriden functions
	void onCollision(Object* other, double timestep) override;
	void onKeyboardInputDown(SDL_Keycode keyCode) override;
	void onKeyboardInputUp(SDL_Keycode keyCode) override;
	void update() override;
	void onMouseClick() override;
};

#endif //_PLAYER_H_