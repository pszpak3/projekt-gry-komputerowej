#ifndef _TEXT_BOARD_H_
#define _TEXT_BOARD_H_
#include "../engine/object.h"
#include "../engine/graphics.h"
#include "../engine/timer.h"

class TextBoard : public Object
{
private:
	Object* m_keyText;
public:
	Timer m_visible_message_timer;
	TextBoard(Graphics* graphics, SceneHandler* sceneHandler);
	void setKeyText(Object* keyText);
	Timer getVisibleMessageTimer();
	Object* getKeyText();

	void onCollision(Object* other, double timestep) override;
	void update() override;
};

#endif //_TEXT_BOARD_H_