#include "../engine/game.h"
#include "../engine/errorHandler.h"
#include "player.h"
#include "wall.h"
#include "background.h"
#include "textBoard.h"
#include "restartScreen.h"
#include "lantern.h"

bool initObjects(Game* game)
{
	Graphics *graphics = game->getGraphics();
	SceneHandler *sceneHandler = game->getSceneHandler();

	//Sounds
	AudioHandler::addMusic(MUSIC_IN_GAME, "music/in_game.wav");
	AudioHandler::playMusic(MUSIC_IN_GAME);
	AudioHandler::addSound(SOUND_FOOTSTEP, "sounds/footstep.wav");
	AudioHandler::addSound(SOUND_PICK_UP, "sounds/pick_up.wav");
	AudioHandler::addSound(SOUND_DYING, "sounds/dying.wav");
	AudioHandler::addSound(SOUND_CHEST_OPEN, "sounds/chest_open.wav");
	AudioHandler::addSound(SOUND_JUMP, "sounds/jump.wav");

	//Sprite sheets
	TextureSpriteSheet walking_tss(1, 5, 46, 60);
	TextureSpriteSheet standing_tss(1, 4, 46, 60);
	TextureSpriteSheet dying_tss(1, 7, 46, 60);
	TextureSpriteSheet rotating_key_tss(1, 12, 15, 34);
	TextureSpriteSheet fire_animation_tss(6, 10, 64, 64);
	TextureSpriteSheet fire_animation_small_tss(6, 10, 45, 45);
	TextureSpriteSheet fire_animation_small_2_tss(6, 10, 45, 45);

	//Loading textures
	graphics->loadTexture(&walking_tss, "sprites/player_walking_right.png");
	graphics->loadTexture(&standing_tss, "sprites/player_standing.png");
	graphics->loadTexture(&dying_tss, "sprites/player_dying.png");
	graphics->loadTexture(&rotating_key_tss, "sprites/key.png");
	graphics->loadTexture(&fire_animation_tss, "sprites/fire1_64.png");
	graphics->loadTexture(&fire_animation_small_tss, "sprites/fire1_64_small.png");
	graphics->loadTexture(&fire_animation_small_2_tss, "sprites/fire1_64_small.png");

	//Loading animations
	Animation* walking_animation = new Animation(walking_tss, 0.1, STATE_WALKING);
	Animation* standing_animation = new Animation(standing_tss, 0.1, STATE_STANDING);
	Animation* dying_animation = new Animation(dying_tss, 0.3, STATE_DYING, false);

	Animation* fire_animation = new Animation(fire_animation_tss, 0.03, STATE_STANDING);
	Animation* fire_animation_small = new Animation(fire_animation_small_tss, 0.03, STATE_STANDING);
	Animation* fire_animation_2_small = new Animation(fire_animation_small_tss, 0.03, STATE_STANDING);

	Animation* rotating_key_animation = new Animation(rotating_key_tss, 0.1, STATE_STANDING);

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////SETTING SCENE ENTITIES////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//Main background
	Background* background = new Background(graphics, sceneHandler);
	background->setTexture("sprites/dark_background.png");

	//Player
	Player* character = new Player(graphics, sceneHandler);
	character->addAnimation(walking_animation);
	character->addAnimation(standing_animation);
	character->addAnimation(dying_animation);
	character->setTexture("sprites/player_jump_1.png", STATE_JUMPING);
	character->setTexture("sprites/player_jump_2.png", STATE_FALLING);
	character->setHitboxForAllFrames();
	character->addType(TYPE_PLAYER);

	//Trunk Axe
	Background* trunk_axe = new Background(graphics, sceneHandler);
	trunk_axe->setTexture("sprites/trunk_axe.png");

	//Bushes
	Background* bush1_1 = new Background(graphics, sceneHandler);
	bush1_1->setTexture("sprites/bush1.png");
	Background* bush1_2 = new Background(graphics, sceneHandler);
	bush1_2->setTexture("sprites/bush1.png");

	Background* bush2_1 = new Background(graphics, sceneHandler);
	bush2_1->setTexture("sprites/bush2.png");
	Background* bush2_2 = new Background(graphics, sceneHandler);
	bush2_2->setTexture("sprites/bush2.png");
	Background* bush3_1 = new Background(graphics, sceneHandler);
	bush3_1->setTexture("sprites/bush3.png");

	//Campfire's fire
	Background* fire = new Background(graphics, sceneHandler);
	fire->addAnimation(fire_animation);

	//Campfire
	Background* fireplace = new Background(graphics, sceneHandler);
	fireplace->setTexture("sprites/fireplace.png");

	//Chest
	Background* chest = new Background(graphics, sceneHandler);
	chest->setTexture("sprites/chest_closed.png", STATE_CHEST_CLOSED);
	chest->setTexture("sprites/chest_opened.png", STATE_CHEST_OPENED);
	chest->setHitboxForAllFrames();
	chest->addType(TYPE_CHEST);

	//Rocks
	Background* rock1_1 = new Background(graphics, sceneHandler);
	rock1_1->setTexture("sprites/rock1.png");
	Background* rock2_1 = new Background(graphics, sceneHandler);
	rock2_1->setTexture("sprites/rock2.png");

	//Key
	Background* key = new Background(graphics, sceneHandler);
	key->addAnimation(rotating_key_animation);
	key->setHitboxForAllFrames();
	key->addType(TYPE_KEY);

	//Well
	Background* well = new Background(graphics, sceneHandler);
	well->setTexture("sprites/well.png");

	//Torches' fire
	Background* fire_small = new Background(graphics, sceneHandler);
	fire_small->addAnimation(fire_animation_small);
	Background* fire_small_2 = new Background(graphics, sceneHandler);
	fire_small_2->addAnimation(fire_animation_small);

	//Torches
	Background* torch1 = new Background(graphics, sceneHandler);
	torch1->setTexture("sprites/torch.png");
	Background* torch2 = new Background(graphics, sceneHandler);
	torch2->setTexture("sprites/torch.png");

	//Lantern
	Lantern* lantern = new Lantern(graphics, sceneHandler);
	lantern->setTexture("sprites/lantern_off_hanging.png", STATE_LANTERN_OFF);
	lantern->setTexture("sprites/lantern_on_hanging.png", STATE_LANTERN_ON);
	lantern->setHitboxForAllFrames();
	lantern->addType(TYPE_LANTERN);

	//Game Over screen
	RestartScreen* looseRestartScreen = new RestartScreen(graphics, sceneHandler, character, key, chest);
	looseRestartScreen->setTextTexture("YOU DIED", "fonts/PixelFJVerdana12pt.ttf", 90, { 255, 0, 0,255 });
	looseRestartScreen->setPosX(525.0);
	looseRestartScreen->setPosY(250.0);

	//Win screen
	RestartScreen* winRestartScreen = new RestartScreen(graphics, sceneHandler, character, key, chest);
	winRestartScreen->setTextTexture("YOU WON!", "fonts/PixelFJVerdana12pt.ttf", 90, { 0, 0, 255,255 });
	winRestartScreen->setPosX(525.0);
	winRestartScreen->setPosY(250.0);

	//End Screen Buttons
	RestartButton* looseRestartButton = new RestartButton(graphics, sceneHandler, character, looseRestartScreen);
	looseRestartButton->setTextTexture("RETRY?", "fonts/PixelFJVerdana12pt.ttf", 35, { 255,0,0,255 });
	looseRestartButton->setPosX(800.0);
	looseRestartButton->setPosY(570.0);
	looseRestartButton->setHitboxForAllFrames();

	RestartButton* winRestartButton = new RestartButton(graphics, sceneHandler, character, winRestartScreen);
	winRestartButton->setTextTexture("PLAY AGAIN", "fonts/PixelFJVerdana12pt.ttf", 35, { 0, 0, 255,255 });
	winRestartButton->setPosX(770.0);
	winRestartButton->setPosY(570.0);
	winRestartButton->setHitboxForAllFrames();

	winRestartScreen->setRestartButton(winRestartButton);
	looseRestartScreen->setRestartButton(looseRestartButton);

	character->setLooseScreen(looseRestartScreen);
	character->setWinScreen(winRestartScreen);

	//Trees
	Background* tree[3] = { 0 };
	for (int i = 0; i < 3; i++)
	{
		tree[i] = new Background(graphics, sceneHandler);
		tree[i]->setTexture("sprites/tree1.png");
	}

	//Grass
	Background* grass1[3] = { 0 };
	for (int i = 0; i < 3; i++)
	{
		grass1[i]= new Background(graphics, sceneHandler);
		grass1[i]->setTexture("sprites/grass1.png");
	}

	//More Grass
	Background* grass2[3] = { 0 };
	for (int i = 0; i < 3; i++)
	{
		grass2[i] = new Background(graphics, sceneHandler);
		grass2[i]->setTexture("sprites/grass2.png");
	}

	//Different ground types
	Wall* ground_full = new Wall(graphics, sceneHandler);
	ground_full->setTexture("sprites/ground_full.png");
	ground_full->setHitboxForAllFrames();
	ground_full->addType(TYPE_GROUND);

	Wall* ground_full_flipped = new Wall(graphics, sceneHandler);
	ground_full_flipped->setTexture("sprites/ground_full_flipped.png");
	ground_full_flipped->setHitboxForAllFrames();

	Wall* ground_full_flipped2 = new Wall(graphics, sceneHandler);
	ground_full_flipped2->setTexture("sprites/ground_full_flipped.png");
	ground_full_flipped2->setHitboxForAllFrames();

	//Ground cut
	Wall* ground_cut[3] = { 0 };
	for (int i = 0; i < 3; i++)
	{
		ground_cut[i] = new Wall(graphics, sceneHandler);
		ground_cut[i]->setTexture("sprites/ground_cut.png");
		ground_cut[i]->setHitboxForAllFrames();
		ground_cut[i]->addType(TYPE_GROUND);
	}

	Wall* ground_cut_580_1 = new Wall(graphics, sceneHandler);
	ground_cut_580_1->setTexture("sprites/ground_cut_580.png");
	ground_cut_580_1->setHitboxForAllFrames();
	ground_cut_580_1->addType(TYPE_GROUND);

	Wall* ground_cut_580_2 = new Wall(graphics, sceneHandler);
	ground_cut_580_2->setTexture("sprites/ground_cut_580.png");
	ground_cut_580_2->setHitboxForAllFrames();
	ground_cut_580_2->addType(TYPE_GROUND);

	//Ground blocks
	Wall* ground_block[6] = { 0 };
	for (int i = 0; i < 6; i++)
	{
		ground_block[i] = new Wall(graphics, sceneHandler);
		ground_block[i]->setTexture("sprites/ground_block.png");
		ground_block[i]->setHitboxForAllFrames();
		ground_block[i]->addType(TYPE_GROUND);
	}

	//Ground platforms
	Wall* ground_platform[7] = { 0 };
	for (int i = 0; i < 7; i++)
	{
		ground_platform[i] = new Wall(graphics, sceneHandler);
		ground_platform[i]->setTexture("sprites/ground_platform.png");
		ground_platform[i]->setHitboxForAllFrames();
		ground_platform[i]->addType(TYPE_GROUND);

	}

	//Boxes
	Wall* box[10] = { 0 };
	for (int i = 0; i < 10; i++)
	{
		box[i] = new Wall(graphics, sceneHandler);
		box[i]->setTexture("sprites/box.png");
		box[i]->setHitboxForAllFrames();
	}

	//Spikes
	Wall* spikes[5] = { NULL };
	for (int i = 0; i < 5; i++)
	{
		spikes[i] = new Wall(graphics, sceneHandler);
		spikes[i]->setTexture("sprites/spikes.png");
		spikes[i]->setHitboxForAllFrames();
		spikes[i]->addType(TYPE_SPIKE);
	}

	//Hint
	Wall* text = new Wall(graphics, sceneHandler);
	text->setTextTexture("Potrzebujesz Klucza", "fonts/PixelFJVerdana12pt.ttf", 20, {255,255,255,255});
	text->addType(TYPE_KEY_TEXT);
	text->setVisibility(false);

	//Textboard
	TextBoard* textBoard = new TextBoard(graphics, sceneHandler);
	textBoard->setTexture("sprites/board.png");
	textBoard->setHitboxForAllFrames();
	textBoard->addType(TYPE_TEXTBOARD);
	textBoard->setKeyText(text);

	//sign config
	Background* sign = new Background(graphics, sceneHandler);
	sign->setTexture("sprites/sign1.png");
	sign->setHitboxForAllFrames();
	sign->addType(TYPE_SIGN);
	sign->addType(TYPE_SIGN_TO_SECOND_MAP);

	//sign2 config
	Background* sign2 = new Background(graphics, sceneHandler);
	sign2->setTexture("sprites/sign1.png");
	sign2->setHitboxForAllFrames();
	sign2->changeAnimation(0, 0.0, SDL_FLIP_HORIZONTAL);
	sign2->addType(TYPE_SIGN);
	sign2->addType(TYPE_SIGN_TO_FIRST_MAP);

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////ADDING OBJECTS TO THE SCENES////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////
	
	//First scene
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(background, 0, 0);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(sign, 1850, 690);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(tree[1], 550, 130);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(tree[2], 1130, 800);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(rock1_1, 700, 723);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(rock2_1, 650, 713);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(fire, 792, 673);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(fireplace, 780, 717);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(trunk_axe, 1130, 798);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(textBoard, 1416, 260);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(bush1_1, 120, 810);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(bush1_2, 930, 310);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(bush2_1, 180, 799);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(bush2_2, 970, 299);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(bush3_1, 250, 798);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(chest, 1826, 292);

	//Character
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(character, 670, 592);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_cut_580_1, 480, 752);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_full_flipped, 1920, 0);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_full_flipped2, -288, 0);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_cut[0], 100, 852);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_cut[1], 960, 852);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_cut_580_2, 1540, 752);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_block[0], 0, 722);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_block[1], 125, 568);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_block[2], 250, 422);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_platform[0], 450, 352);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_platform[1], 888, 352);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_platform[2], 1326, 352);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(ground_platform[3], 1726, 352);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(tree[0], 400, 610);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass1[0], 280, 407);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass1[1], 1426, 337);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass1[2], 1650, 737);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass2[0], 20, 705);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass2[1], 1526, 335);
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(grass2[2], 1740, 735);

	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(text, 20, 20);

	//Second scene
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(background, 0, 0);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(sign2, 50, 790);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(character);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(key, 50, 210);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_full, 0, 852);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_full_flipped, 1920, 0);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_full_flipped2, -288, 0);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_cut[2], 0, 252);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_block[3], 1826, 722);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_block[4], 980, 400);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_block[5], 1626, 622);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_platform[4], 1200, 490);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(ground_platform[5], 630, 252);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(fire_small, 86, 354);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(torch1, 100, 392);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(fire_small_2, 334, 354);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(torch2, 348, 392);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[0], 480, 784);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[1], 548, 784);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[2], 616, 784);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[3], 684, 784);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[4], 548, 716);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[5], 616, 716);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[6], 684, 716);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[7], 616, 648);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[8], 684, 648);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(box[9], 684, 580);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(spikes[0], 762, 822);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(spikes[1], 831, 822);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(spikes[2], 900, 822);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(spikes[3], 136, 222);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(spikes[4], 304, 222);

	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(well, 1092, 715);
	sceneHandler->getScene(SCENE_SECOND_MAP)->addObject(lantern, 1250, 344);

	return true;
}

int main(int argc, char **args)
{
	Game *game = new Game("Adventure");

	game->init();
	initObjects(game);
	game->run();
	game->cleanup();

	return 0;
}
