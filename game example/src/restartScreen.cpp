#include "restartScreen.h"

RestartScreen::RestartScreen(Graphics* graphics, SceneHandler* sceneHandler, Player* player, Background* _key, Background* _chest) : Object(graphics, sceneHandler), key(_key), chest(_chest) {}

void RestartScreen::show()
{
	getSceneHandler()->getCurrentScene()->addObject(this);
	getSceneHandler()->getCurrentScene()->addObject(restartButton);
}

void RestartScreen::hide()
{
	restartButton->deleteFromScene(SCENE_FIRST_MAP);
	restartButton->deleteFromScene(SCENE_SECOND_MAP);

	this->deleteFromScene(SCENE_FIRST_MAP);
	this->deleteFromScene(SCENE_SECOND_MAP);
}

Background* RestartScreen::getKey()
{
	return key;
}

Background* RestartScreen::getChest()
{
	return chest;
}

void RestartScreen::setRestartButton(RestartButton* _restartButton)
{
	restartButton = _restartButton;
}
