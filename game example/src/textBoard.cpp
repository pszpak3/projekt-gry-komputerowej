#include "textBoard.h"

TextBoard::TextBoard(Graphics* graphics, SceneHandler* sceneHandler) : Object(graphics, sceneHandler)
{}

void TextBoard::onCollision(Object* other, double timestep)
{
	if (other->isOfType(TYPE_PLAYER))
	{
		this->getKeyText()->setVisibility(true);
		m_visible_message_timer.start();
	}
}

void TextBoard::setKeyText(Object* keyText)
{
	this->m_keyText = keyText;
}

Timer TextBoard::getVisibleMessageTimer()
{
	return m_visible_message_timer;
}

Object* TextBoard::getKeyText()
{
	return m_keyText;
}

void TextBoard::update()
{

	if(m_visible_message_timer.getTimeInSeconds() > 0.1)
	{
		this->getKeyText()->setVisibility(false);
		m_visible_message_timer.stop();
	}
}

