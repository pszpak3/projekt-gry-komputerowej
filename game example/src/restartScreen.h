#ifndef _RESTART_SCREEN_H_
#define _RESTART_SCREEN_H_
#include "../engine/object.h"
#include "../engine/graphics.h"
#include "restartButton.h"
#include "background.h"

class Player;
class RestartButton;
class Background;

class RestartScreen : public Object
{
	RestartButton* restartButton = NULL;
	Background* key = NULL;
	Background* chest = NULL;

public:
	RestartScreen(Graphics* graphics, SceneHandler* sceneHandler, Player* player, Background* key, Background* chest);
	void show();
	void hide();
	Background* getKey();
	Background* getChest();
	void setRestartButton(RestartButton* restartButton);
};

#endif //_RESTART_SCREEN_H_