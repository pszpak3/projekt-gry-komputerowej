#ifndef ANIMATION_H
#define ANIMATION_H
#include <SDL.h>
#include <vector>
#include <cassert>
#include "textureSpriteSheet.h"
#include "frame.h"
#include "constants.h"
#include "texture.h"

class Animation
{
private:
	TextureSpriteSheet m_tss;
	List<Frame *> m_frames;
	int m_current_frame;

	//Amount of seconds needed to change animation clip.
	double m_seconds_to_change_clip;
	double m_animation_passed_time;
	int m_state_name;
	bool m_loop;

public:
	Animation(TextureSpriteSheet tss, double seconds_to_change_clip, int state_name, bool loop = true);
	~Animation();
	Texture getCurrentTexture(double time);
	Frame * getCurrentFrame();
	double getSecondsToChangeClip();
	void nextFrame();
	void reset();
	bool isLooping();
	void setLooping(bool loop);
	int getStateName();
	List<Frame*>* getFrameList();
	TextureSpriteSheet* getTextureSpriteSheet();
};
#endif //ANIMATION_H