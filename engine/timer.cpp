#include "timer.h"

Timer::Timer()
{
	m_started_passed_time = 0;
	m_paused_passed_time = 0;
	m_start_state = false;
	m_pause_state = false;
}

Timer::~Timer()
{
}

void Timer::start()
{
	m_start_state = true;
	m_pause_state = false;
	m_started_passed_time = SDL_GetTicks();
	m_paused_passed_time = 0;
}

void Timer::stop() 
{
	m_start_state = false;
	m_pause_state = false;
	m_started_passed_time = 0;
	m_paused_passed_time = 0;
}

void Timer::pause() 
{
	if (m_start_state && !m_pause_state) 
	{
		m_pause_state = true;
		m_paused_passed_time = SDL_GetTicks() - m_started_passed_time;
		m_started_passed_time = 0;
	}
}

void Timer::unpause()
{
	if (m_start_state && m_pause_state)
	{
		m_pause_state = false;
		m_started_passed_time = SDL_GetTicks() - m_paused_passed_time;
		m_paused_passed_time = 0;
	}
}

Uint32 Timer::getTimeInMiliseconds() 
{
	Uint32 time = 0;

	if (m_start_state)
	{
		if (m_pause_state)
		{
			time = m_paused_passed_time;
		}
		else
		{
			time = SDL_GetTicks() - m_started_passed_time;
		}
	}

	return time;
}

double Timer::getTimeInSeconds()
{
	Uint32 time = 0;

	if (m_start_state)
	{
		if (m_pause_state)
		{
			time = m_paused_passed_time;
		}
		else
		{
			time = SDL_GetTicks() - m_started_passed_time;
		}
	}

	return time / 1000.0;
}

bool Timer::isStarted()
{
	return m_start_state;
}

bool Timer::isPaused()
{
	return m_pause_state;
}