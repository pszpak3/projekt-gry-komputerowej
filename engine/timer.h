#ifndef TIMER_H
#define TIMER_H
#include <SDL.h>

class Timer
{
private:
	//Time that passed when the timer has started.
	Uint32 m_started_passed_time;
	//Time that passed when the timer has paused (time between starting the timer and pausing it).
	Uint32 m_paused_passed_time;
	//Tells whether the timer is started.
	bool m_start_state;
	//Tells whether the timer is paused.
	bool m_pause_state;

public:
	Timer();
	~Timer();
	void start();
	void stop();
	void pause();
	void unpause();
	bool isStarted();
	bool isPaused();
	Uint32 getTimeInMiliseconds();
	double getTimeInSeconds();
};

#endif