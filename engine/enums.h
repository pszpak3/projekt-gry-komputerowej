#ifndef ENUMS_H
#define ENUMS_H

enum SceneEnum
{
	SCENE_FIRST_MAP,
	SCENE_TOTAL
};

enum DirectionEnums
{
	DIRECTION_NO = 0,
	DIRECTION_LEFT = 1,
	DIRECTION_UP = 2,
	DIRECTION_RIGHT = 4,
	DIRECTION_DOWN = 8,
};

enum TypesEnum
{
	TYPE_DEFAULT,
	TYPE_TOTAL
};

#endif //ENUMS_H