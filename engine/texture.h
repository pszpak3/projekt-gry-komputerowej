#ifndef TEXTURE_H
#define TEXTURE_H
#include <SDL.h>
class Texture
{
public:
	int m_x;
	int m_y;
	int m_w;
	int m_h;
	SDL_Texture * m_texture;
public:
	Texture(int x, int y, int w, int h, SDL_Texture* texture);
	Texture();
};
#endif //TEXTURE_H

