//object animation list
#ifndef ANIMATION_HANDLER_H
#define ANIMATION_HANDLER_H
#include <SDL.h>
#include <vector>
#include "animation.h"

class AnimationHandler
{
private:
	//object animation list
	List<Animation *> m_animations;
	double m_angle;
	SDL_RendererFlip m_flip;
	int m_current_animation_state_name;
public:
	AnimationHandler();
	~AnimationHandler();

	void addAnimation(Animation *animation);
	void setCurrentAnimation(int state_name);
	Animation *getCurrentAnimation();
	Animation* getAnimation(int state_name);

	void setFlip(SDL_RendererFlip flip);
	void setAngle(double angle);
	SDL_RendererFlip getFlip();
	double getAngle();

	//Sets hitboxes for all frames the same as the frame parameters.
	void setHitboxForAllFrames();
};
#endif //ANIMATION_HANDLER_H
