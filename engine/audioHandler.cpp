#include <SDL.h>
#include "audioHandler.h"
#include "errorHandler.h"

bool AudioHandler::initializeSDLAudio(int frequency, Uint16 format, int channels, int soundSize)
{
	if (SDL_Init(SDL_INIT_AUDIO) < 0) 
	{
		ErrorHandler::errorMessageBox("SDL could not initialize audio!", SDL_GetError());
		return false;
	}
	
	if (Mix_OpenAudio(frequency, format, channels, soundSize) < 0) 
	{
		ErrorHandler::errorMessageBox("SDL could not initialize audio mixer!", SDL_GetError());
		return false;
	}

	return true;
}

bool AudioHandler::addMusic(int id, std::string path)
{
	Mix_Music * loaded_music = Mix_LoadMUS(path.c_str());
	if (loaded_music == NULL)
	{	
		ErrorHandler::errorMessageBox("Unable to load music!", SDL_GetError());
		return false;
	}

	Music music = Music(id, loaded_music);
	m_music_list.push_back(music);
	return true;
}

bool AudioHandler::addSound(int id, std::string path)
{
	Mix_Chunk* loaded_sound = Mix_LoadWAV(path.c_str());
	if (loaded_sound == NULL)
	{
		ErrorHandler::errorMessageBox("Unable to load sound effect!", SDL_GetError());
		return false;
	}

	Sound sound = Sound(id, -2, loaded_sound);
	m_sound_list.push_back(sound);
	return true;
}

void AudioHandler::playMusic(int id, int timesRepeated)
{
	bool wasFound = false;

	for (unsigned int i = 0; i < m_music_list.size(); i++) 
	{
		if (m_music_list.at(i).id == id) 
		{
			Mix_PlayMusic(m_music_list.at(i).music, timesRepeated);
			wasFound = true;
			break;
		}
	}

	if (!wasFound) 
	{
		ErrorHandler::errorMessageBox("Cannot find music with id: " + id, SDL_GetError());
	}
}

void AudioHandler::stopCurrentMusic()
{
	if (Mix_PlayingMusic != 0) 
	{
		Mix_HaltMusic();
	}
}

void AudioHandler::pauseCurrentMusic()
{
	if (Mix_PlayingMusic != 0) 
	{
		Mix_PauseMusic();
	}
}

void AudioHandler::unpauseCurrentMusic()
{
	if (Mix_PausedMusic() == 1) 
	{
		Mix_ResumeMusic();
	}
}

void AudioHandler::playSound(int id, bool wait_for_channel_to_finish, int timesRepeated)
{

	for (unsigned int i = 0; i < m_sound_list.size(); i++) 
	{
		if (m_sound_list.at(i).id == id)
		{
			if (wait_for_channel_to_finish)
			{
				if (m_sound_list.at(i).channel == -2)
				{
					m_sound_list.at(i).channel = Mix_PlayChannel(-1, m_sound_list.at(i).sound, timesRepeated);
					return;
				}
				if (Mix_Playing(m_sound_list.at(i).channel) != 1)
				{
					m_sound_list.at(i).channel = Mix_PlayChannel(-1, m_sound_list.at(i).sound, timesRepeated);
					Mix_ChannelFinished(deleteChanellFromSound);
					return;
				}
				return;
			}
			Mix_PlayChannel(-1, m_sound_list.at(i).sound, timesRepeated);
			return;
		}
	}

	ErrorHandler::errorMessageBox("Cannot find sound with id: " + id, SDL_GetError());
}

void AudioHandler::deleteChanellFromSound(int channel)
{
	for (unsigned int i = 0; i < m_sound_list.size(); i++)
	{
		if (m_sound_list.at(i).channel == channel)
		{
			m_sound_list.at(i).channel = -2;
		}
	}
}
