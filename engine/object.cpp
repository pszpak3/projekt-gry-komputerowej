#include "object.h"

unsigned int Object::getNewId()
{
	return ++m_objects_id_setter;
}

bool Object::deleteFromScene(Scene* scene, bool permanently)
{
	for (int i = 0; i < scene->getSize(); i++)
	{
		if (scene->getObjectDatas()[i].object == this)
		{
			scene->getObjectDatas()[i].toDelete = true;
			if (permanently)
				scene->getObjectDatas()[i].deletePermanent = true;
			return true;
		}
	}
	return false;
}

Object::Object(Graphics *graphics, SceneHandler* sceneHandler) : m_graphics(graphics), m_sceneHandler(sceneHandler),
																 m_visibility(true),
																 m_pos_x(0.0), m_pos_y(0.0),
																 m_current_vel_x(0.0), m_current_vel_y(0.0), m_walking_vel(300.0),
																 m_acceleration_y(0.0)
{
	m_animation_handler = new AnimationHandler();
	m_id = getNewId();
}

Object::~Object()
{
	delete m_animation_handler;
}

SceneHandler* Object::getSceneHandler()
{
	return m_sceneHandler;
}

AnimationHandler* Object::getAnimationHandler()
{
	return m_animation_handler;
}

double Object::getAccelerationY()
{
	return m_acceleration_y;
}

void Object::setAccelerationY(double new_acceleration)
{
	m_acceleration_y = new_acceleration;
}

void Object::setPosX(double pos_x) {
	this->m_pos_x = pos_x;
}

void Object::setPosY(double pos_y) {
	this->m_pos_y = pos_y;
}

double Object::getPosX() {
	return this->m_pos_x;
}

double Object::getPosY() {
	return this->m_pos_y;
}

bool Object::isVisible()
{
	return m_visibility;
}

void Object::setVisibility(bool new_visibility)
{
	m_visibility = new_visibility;
}

bool Object::deleteFromScene(SceneEnum scene)
{
	return deleteFromScene(getSceneHandler()->getScene(scene));
}

bool Object::deleteFromCurrentScene()
{
	return deleteFromScene(getSceneHandler()->getCurrentScene());
}


void Object::deletePermanently()
{
	for (int i = 0; i < SCENE_TOTAL; i++)
	{
		deleteFromScene(getSceneHandler()->getAllScenes() + i, true);
	}
}

HitBox Object::getMaxRangeHitbox()
{
	return maxRangeHitbox;
}

void Object::setMaxRangeHitbox()
{
	if (!getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->empty())
	{
		HitBox* first_hitbox = this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(0);
		int max_left = first_hitbox->getX();
		int max_up = first_hitbox->getY();
		for (int i = 1; i < this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->size(); i++)
		{
			HitBox* hitbox = this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(i);
			if (hitbox->getX() < max_left)
				max_left = hitbox->getX();
			if (hitbox->getY() < max_up)
				max_up = hitbox->getY();
		}
		int max_right = first_hitbox->getX() + first_hitbox->getW();
		int left_of_max_right = first_hitbox->getX();
		int max_down = first_hitbox->getY() + first_hitbox->getH();
		int up_of_max_down = first_hitbox->getY();
		for (int i = 1; i < this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->size(); i++)
		{
			HitBox* hitbox = this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(i);
			if (hitbox->getX() + hitbox->getW() > max_right)
			{
				max_right = hitbox->getX() + hitbox->getW();
				left_of_max_right = hitbox->getX();
			}

			if (hitbox->getY() + hitbox->getH() > max_down)
			{
				max_down = hitbox->getY() + hitbox->getH();
				up_of_max_down = hitbox->getY();
			}
		}
		maxRangeHitbox.set(max_left, max_up, max_right - left_of_max_right, max_down - up_of_max_down);
	}
}

bool Object::touchesFromBelow(Object* other)
{
	if ((int)getPosY() + getMaxRangeHitbox().getY() + getMaxRangeHitbox().getH() == (int)other->getPosY() + other->getMaxRangeHitbox().getY())
		return true;
	return false;
}

bool Object::touchesLowerBound()
{
	if ((int)getPosY() + getMaxRangeHitbox().getY() + getMaxRangeHitbox().getH() >= getGraphics()->getLowerBound())
		return true;
	return false;
}


double Object::getWalkingVel()
{
	return m_walking_vel;
}

bool Object::collides(int x1, int y1, int h1, int w1, int x2, int y2, int h2, int w2)
{
	int top1 = y1;
	int top2 = y2;
	int bottom1 = y1 + h1;
	int bottom2 = y2 + h2;
	int left1 = x1;
	int left2 = x2;
	int right1 = x1 + w1;
	int right2 = x2 + w2;
	if (bottom1 <= top2) return false;
	if (bottom2 <= top1) return false;
	if (right1 <= left2) return false;
	if (right2 <= left1) return false;
	return true;
}

bool Object::collidesWith(Object * other)
{
	for (int i = 0; i < this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->size(); i++)
	{
		for (int j = 0; j < other->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->size(); j++)
		{
			HitBox* thisHitBox = this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(i);
			HitBox * otherHitBox = other->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(j);

			if (collides((int)this->getPosX() + thisHitBox->getX(), (int)this->getPosY() + thisHitBox->getY(), thisHitBox->getH(), thisHitBox->getW(),
				(int)other->getPosX() + otherHitBox->getX(), (int)other->getPosY() + otherHitBox->getY(), otherHitBox->getH(), otherHitBox->getW()))
				return true;
		}
	}
	return false;
}

void Object::stop(Object* object_to_be_stopped_from, double timestep)
{
	this->setPosY(this->getPosY() - (this->getVelY() * timestep));

	if (collidesWith(object_to_be_stopped_from))
	{
		this->setPosX(this->getPosX() - (this->getVelX() * timestep));
	}
	
	this->setPosY(this->getPosY() + (this->getVelY() * timestep));

	if (collidesWith(object_to_be_stopped_from))
	{
		this->setPosY(this->getPosY() - (this->getVelY() * timestep));
		m_current_vel_y = 0.0;
	}
}

void Object::setHitboxForAllFrames()
{
	m_animation_handler->setHitboxForAllFrames();
}

Graphics * Object::getGraphics()
{
	return m_graphics;
}

void Object::addAnimation(Animation * animation)
{
	m_animation_handler->addAnimation(animation);
}

Animation* Object::getCurrentAnimation()
{
	return m_animation_handler->getCurrentAnimation();
}

void Object::changeAnimation(int state_name, double angle, SDL_RendererFlip flip)
{
	m_animation_handler->setCurrentAnimation(state_name);
	if (m_animation_handler->getCurrentAnimation()->getSecondsToChangeClip() < 0)
		setCurrentTexture(m_animation_handler->getCurrentAnimation()->getCurrentTexture(0.0));
	m_animation_handler->setAngle(angle);
	m_animation_handler->setFlip(flip);
}

void Object::setCurrentTexture(Texture currentTexture)
{
	m_current_texture = currentTexture;
}

void Object::setSingleFrameForAnimation(Texture texture, int state_name)
{
	TextureSpriteSheet tss(1, 1, texture.m_w, texture.m_h);
	tss.setSpriteSheetTexture(texture.m_texture);
	Animation* animation = new Animation(tss, -1.0, state_name);
	this->addAnimation(animation);
	if (m_current_texture.m_texture == NULL)
	{
		setCurrentTexture(texture);
	}
}

void Object::setTexture(std::string path, int state_name)
{
	Texture texture = m_graphics->loadTexture(path);
	setSingleFrameForAnimation(texture, state_name);
}

void Object::setTextTexture(std::string text, std::string font_path, int font_size, SDL_Color font_color, int state_name)
{
	Texture texture = m_graphics->loadTextTexture(text, font_path, font_size, font_color);
	setSingleFrameForAnimation(texture, state_name);
}

int Object::getId()
{
	return m_id;
}

void Object::addType(unsigned int type)
{
	assert(type < TYPE_TOTAL);
	m_types.push_back(type);
}

void Object::removeType(unsigned int type)
{
	for (int i = 0; i < m_types.size(); i++)
	{
		if (m_types[i] == type)
		{
			m_types.erase(m_types.begin() + i);
			return;
		}
	}
}

Uint8 Object::getVelocityDirection()
{
	Uint8 ret_direction = DIRECTION_NO;
	if (this->getVelX() < 0.0)
		ret_direction |= DIRECTION_LEFT;
	if (this->getVelX() > 0.0)
		ret_direction |= DIRECTION_RIGHT;
	if (this->getVelY() < 0.0)
		ret_direction |= DIRECTION_UP;
	if (this->getVelY() > 0.0)
		ret_direction |= DIRECTION_DOWN;
	return ret_direction;
}

void Object::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		onKeyboardInputDown(e.key.keysym.sym);
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		onKeyboardInputUp(e.key.keysym.sym);
	}
	//If mouse event happened
	else if (e.type == SDL_MOUSEBUTTONDOWN)
	{
		if (cursorAtObject(e))
		{
			onMouseClick();
		}
	}
	//Handle mouse hover
	handleMouseHover(e);
}

void Object::handleCollision(Object* other, double timestep)
{
	if (collidesWith(other))
	{
		onCollision(other, timestep);
	}
}

void Object::handleMouseHover(SDL_Event &e)
{
	if (cursorAtObject(e))
	{
		onMouseHover();
	}
}

bool Object::cursorAtObject(SDL_Event& e)
{
	int cursor_x = e.motion.x;
	int cursor_y = e.motion.y;
	//Get object's hitbox
	for (int i = 0; i < this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->size(); i++)
	{
		HitBox* thisHitBox = this->getCurrentAnimation()->getCurrentFrame()->getCollisionHitboxList()->at(i);
		int x = ((int)this->getPosX() + thisHitBox->getX());
		int y = ((int)this->getPosY() + thisHitBox->getY());
		int w = thisHitBox->getW();
		int h = thisHitBox->getH();

		if (!m_graphics->getLetterBoxing())
		{
			x *= m_graphics->getWidthScalingRatio();
			y *= m_graphics->getHeightScalingRatio();
			w *= m_graphics->getWidthScalingRatio();
			h *= m_graphics->getHeightScalingRatio();
		}

		if (cursorCollides(cursor_x, cursor_y, x, y, w, h))
			return true;
	}
	return false;
}

bool Object::cursorCollides(int cursor_x, int cursor_y, int x, int y, int w, int h)
{
	//Mouse is left of the button
	if (cursor_x < x) return false;
	//Mouse is right of the button
	if (cursor_x > x + w) return false;
	//Mouse above the button
	if (cursor_y < y) return false;
	//Mouse below the button
	if (cursor_y > y + h) return false;
	return true;
}

void Object::update() {}

void Object::onKeyboardInputUp(SDL_Keycode keyCode) {}

void Object::onKeyboardInputDown(SDL_Keycode keyCode) {}

void Object::onMouseHover() {}

void Object::onMouseClick() {}

void Object::onCollision(Object* other, double timestep) {}

bool Object::isOfType(unsigned int type)
{
	for (auto t : m_types)
	{
		if (t == type)
			return true;
	}
	return false;
}

void Object::updateAnimation(double time)
{
	if (m_animation_handler->getCurrentAnimation()->getSecondsToChangeClip() > 0)
	{
		setCurrentTexture(m_animation_handler->getCurrentAnimation()->getCurrentTexture(time));
	}
}

void Object::updatePosition(double timeStep)
{
	//Move the object left or right
	m_pos_x += m_current_vel_x * timeStep;

	//Move the object up or down; Apply the custom acceleration
	m_current_vel_y += m_acceleration_y * timeStep;
	m_pos_y += m_current_vel_y * timeStep;
}

void Object::setVelX(double vel_x) 
{
	m_current_vel_x = vel_x;
}

void Object::setVelY(double vel_y)
{
	m_current_vel_y = vel_y;
}

void Object::setWalkingVel(double walking_vel)
{
	m_walking_vel = walking_vel;
}

double Object::getVelX()
{
	return m_current_vel_x;
}

double Object::getVelY()
{
	return m_current_vel_y;
}

void Object::render()
{
	//Specific clip from the SpiteSheet texture.
	SDL_Rect clip = { m_current_texture.m_x, m_current_texture.m_y, m_current_texture.m_w, m_current_texture.m_h };
	//Has information about the position of a texture on the screen and dimensions of it.
	SDL_Rect renderQuad = { (int)m_pos_x, (int)m_pos_y, m_current_texture.m_w, m_current_texture.m_h };

	SDL_Point center;
	center.x = m_current_texture.m_w / 2;
	center.y = m_current_texture.m_h / 2;
	m_graphics->render(m_current_texture.m_texture, &clip, &renderQuad, m_animation_handler->getAngle(), &center, m_animation_handler->getFlip());
}
