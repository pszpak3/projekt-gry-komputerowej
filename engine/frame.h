#ifndef FRAME_H
#define FRAME_H
#include "TextureSpriteSheet.h"
#include "texture.h"
#include "constants.h"
#include "hitBox.h"

class Frame
{
public:
	int m_col;
	int m_row;
	List<HitBox *> m_collisionHitboxes;
public:
	Frame(int x, int y);
	List<HitBox *> * getCollisionHitboxList();
};
#endif //FRAME_H
