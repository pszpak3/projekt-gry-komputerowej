#ifndef _MUSIC_H_
#define	_MUSIC_H_
#include <SDL_mixer.h>

struct Music
{
	int id;
	Mix_Music* music;
	Music(int id, Mix_Music* music);
};

#endif