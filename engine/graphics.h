#ifndef _GRAPHICS_H_
#define	_GRAPHICS_H_
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <SDL_ttf.h>
#include <cassert>
#include "texture.h"
#include "errorHandler.h"
#include "textureSpriteSheet.h"
#include "constants.h"

class Graphics
{
private:
	std::string window_name;
	const int m_logical_screen_width;
	const int m_logical_screen_height;
	int m_current_screen_width;
	int m_current_screen_height;
	SDL_Window *m_window = NULL;
	SDL_Renderer *m_renderer = NULL;
	SDL_Texture* m_back_buffer = NULL;
	bool m_letterboxing = false;
	//Scaling ratios for mouse events
	float m_scaling_ratio_width = 1.0f;
	float m_scaling_ratio_height = 1.0f;

public:
	Graphics(const char* window_name, int game_width, int game_height);
	~Graphics();
	int getUpperBound();
	int getLowerBound();
	int getRightBound();
	int getLeftBound();
	int getScreenWidth(); 
	int getScreenHeight();
	float getWidthScalingRatio();
	float getHeightScalingRatio();
	bool initializeSDL();
	bool createWindow();
	void getNativeDesktopResolution(int *width, int *height);
	void setResolution(int width, int height);
	//Scaling quality when screen resizing happens (NEAREST, LINEAR, ANISOTROPIC).
	void setScalingQuality(ScalingQualityEnum scaling_quality);
	//Copy resized (if neccessary) back buffer texture to the window.
	void renderBackBufferToWindow();
	//Letterboxing on/off
	void setLetterBoxing(bool value);
	bool getLetterBoxing();
	//Texture loading
	void loadTexture(TextureSpriteSheet *texture, std::string path);
	Texture loadTexture(std::string path);
	Texture loadTextTexture(std::string text, std::string font_path, int font_size, SDL_Color font_color);
	//Renders texture at given point (x,y) 
	void render(SDL_Texture *texture, SDL_Rect* clip, SDL_Rect* renderQuad, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
	void renderUpdate();
	void renderClear();
	void cleanup();
};

#endif