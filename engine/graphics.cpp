#include "graphics.h"
#include "textureSpriteSheet.h"

Graphics::Graphics(const char* _window_name, int _game_width, int _game_height)
	 : window_name(_window_name), m_logical_screen_width(_game_width), m_logical_screen_height(_game_height)
{}

int Graphics::getUpperBound()
{
	return 0;
}

int Graphics::getLowerBound()
{
	return m_logical_screen_height;
}

bool Graphics::initializeSDL()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		//SDL could not initialize!
		ErrorHandler::errorMessageBox("SDL could not initialize!", SDL_GetError());
		return false;
	}

	return true;
}

bool Graphics::createWindow()
{
	m_window = SDL_CreateWindow(window_name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_logical_screen_width, m_logical_screen_height, SDL_WINDOW_FULLSCREEN);
	if (m_window == NULL)
	{
        //Window could not be created!
		ErrorHandler::errorMessageBox("Window could not be created!","SDL Error:\n"+std::string(SDL_GetError()));
		return false;
	}
	else
	{
		m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
		if (m_renderer == NULL)
		{
			ErrorHandler::errorMessageBox("Renderer could not be created!","SDL Error:\n"+std::string(SDL_GetError()));
			return false;
		}
		else
		{

			//Run game in desktop native resolution
			int screen_width, screen_height;
			getNativeDesktopResolution(&screen_width, &screen_height);
			setResolution(screen_width, screen_height);

			//Initialize renderer color (RGB)
			SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 0);

			//Scaling quality when texture resizing happens
			setScalingQuality(ANISOTROPIC);

			//Initialize back buffer
			m_back_buffer = SDL_CreateTexture(m_renderer, SDL_GetWindowPixelFormat(m_window), SDL_TEXTUREACCESS_TARGET, m_logical_screen_width, m_logical_screen_height);

			//Set render target
			SDL_SetRenderTarget(m_renderer, m_back_buffer);

			//Initialize PNG loading
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) & imgFlags))
			{
				ErrorHandler::errorMessageBox("SDL_Image could not initialize!","SDL Image Error:\n"+std::string(IMG_GetError()));
				return false;
			}

			//Initialize SDL_ttf
			if (TTF_Init() == -1)
			{
				ErrorHandler::errorMessageBox("Unable to initialize SDL_ttf!", "SDL_TTF Error:\n" + std::string(TTF_GetError()));
				return false;
			}
		}
	}

	return true;
}

void Graphics::getNativeDesktopResolution(int *width, int *height)
{
	SDL_DisplayMode display_mode;
	if (SDL_GetDesktopDisplayMode(0, &display_mode) != 0)
	{
		ErrorHandler::errorMessageBox("Cannot get native desktop screen resolution", "SDL Error:\n" + std::string(SDL_GetError()));
		*width = -1;
		*height = -1;
		return;
	}

	*width = display_mode.w;
	*height = display_mode.h;
}

int Graphics::getRightBound()
{
	return m_logical_screen_width;
}

int Graphics::getLeftBound()
{
	return 0;
}

int Graphics::getScreenWidth()
{
	return m_current_screen_width;
}

int Graphics::getScreenHeight()
{
	return m_current_screen_height;
}

float Graphics::getWidthScalingRatio()
{
	return m_scaling_ratio_width;
}

float Graphics::getHeightScalingRatio()
{
	return m_scaling_ratio_height;
}

void Graphics::setResolution(int width, int height)
{
	assert(width > 0 && height > 0);
	SDL_SetWindowSize(m_window, width, height);
	m_current_screen_width = width;
	m_current_screen_height = height;
	m_scaling_ratio_width = width / (float)m_logical_screen_width;
	m_scaling_ratio_height = height / (float)m_logical_screen_height;
}

void Graphics::setScalingQuality(ScalingQualityEnum scaling_quality)
{
	assert(scaling_quality != NEAREST || scaling_quality != LINEAR || scaling_quality != ANISOTROPIC);
	if (scaling_quality == NEAREST)
	{
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
	}
	else if (scaling_quality == LINEAR)
	{
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
	}
	else if (scaling_quality == ANISOTROPIC)
	{
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "best");
	}
	else
	{
		ErrorHandler::errorMessageBox("Bad parameter for scaling quality function.", SDL_GetError());
	}

	//Recreate the back buffer so that change actually happens.
	SDL_DestroyTexture(m_back_buffer);
	m_back_buffer = SDL_CreateTexture(m_renderer, SDL_GetWindowPixelFormat(m_window), SDL_TEXTUREACCESS_TARGET, m_logical_screen_width, m_logical_screen_height);
}

void Graphics::loadTexture(TextureSpriteSheet *tss, std::string path)
{
	SDL_Surface* loaded_surface = IMG_Load(path.c_str());
	if (loaded_surface == NULL) {
		ErrorHandler::errorMessageBox("Unable to load image!", "SDL Image Error:\n" + std::string(IMG_GetError()));
	}
	else
	{
		tss->setSpriteSheetTexture(SDL_CreateTextureFromSurface(m_renderer, loaded_surface));
		if (tss->getSpriteSheetTexture() == NULL)
		{
			ErrorHandler::errorMessageBox("Unable to create texture!", "SDL Image Error:\n" + std::string(IMG_GetError()));
		}
		
		//Cleaning up lodaded_surface
		SDL_FreeSurface(loaded_surface);
	}

}

Texture Graphics::loadTexture(std::string path)
{
	SDL_Surface* loaded_surface = IMG_Load(path.c_str());
	if (loaded_surface == NULL) {
		ErrorHandler::errorMessageBox("Unable to load image!", "SDL Image Error:\n" + std::string(IMG_GetError()));
		return Texture();
	}

	SDL_Texture* raw_texture = SDL_CreateTextureFromSurface(m_renderer, loaded_surface);

	if (raw_texture == NULL)
	{
		ErrorHandler::errorMessageBox("Unable to create texture!", "SDL Image Error:\n" + std::string(IMG_GetError()));
		SDL_FreeSurface(loaded_surface);
		return Texture();
	}

	Texture texture(0, 0, loaded_surface->w, loaded_surface->h, raw_texture);
	//Cleaning up lodaded_surface
	SDL_FreeSurface(loaded_surface);
	return texture;
}

Texture Graphics::loadTextTexture(std::string text, std::string font_path, int font_size, SDL_Color font_color)
{
	TTF_Font* font = TTF_OpenFont(font_path.c_str(), font_size);
	SDL_Surface* text_surface = TTF_RenderText_Solid(font, text.c_str(), font_color);
	if (text_surface == NULL)
	{
		ErrorHandler::errorMessageBox("Unable to create text surface!", "SDL_TTF Error:\n" + std::string(SDL_GetError()));
		return Texture();
	}

	SDL_Texture *raw_texture = SDL_CreateTextureFromSurface(m_renderer, text_surface);

	if (raw_texture == NULL)
	{
		ErrorHandler::errorMessageBox("Unable to create texture from rendered text!", "SDL Error: %s\n" + std::string(SDL_GetError()));
		SDL_FreeSurface(text_surface);
		return Texture();
	}

	Texture texture(0, 0, text_surface->w, text_surface->h, raw_texture);
	//Get rid of old surface
	SDL_FreeSurface(text_surface);
	return texture;
}

void Graphics::setLetterBoxing(bool value)
{
	if (value)
	{
		m_letterboxing = true;
		SDL_RenderSetLogicalSize(m_renderer, m_logical_screen_width, m_logical_screen_height);
	}
	else
	{
		m_letterboxing = false;
		SDL_RenderSetLogicalSize(m_renderer, 0, 0);
	}
}

bool Graphics::getLetterBoxing()
{
	return m_letterboxing;
}

void Graphics::renderClear()
{
	//Clear the window.
	SDL_RenderClear(m_renderer);
	SDL_SetRenderTarget(m_renderer, m_back_buffer);
	//Clear the back buffer.
	SDL_RenderClear(m_renderer);
}

void Graphics::render(SDL_Texture *texture, SDL_Rect* clip, SDL_Rect *renderQuad, double angle, SDL_Point *center, SDL_RendererFlip flip)
{
	SDL_RenderCopyEx(m_renderer, texture, clip, renderQuad, angle, center, flip);
}

void Graphics::renderBackBufferToWindow()
{
	SDL_Rect logical_size, new_size;
	logical_size = { 0, 0, m_logical_screen_width, m_logical_screen_height };

	if (m_letterboxing)
	{
		new_size = { 0, 0, m_logical_screen_width, m_logical_screen_height };
	}
	else
	{
		new_size = { 0, 0, m_current_screen_width, m_current_screen_height };
	}
	
	//Set target to the window.
	SDL_SetRenderTarget(m_renderer, NULL);
	//Render from back buffer to the window.
	SDL_RenderCopy(m_renderer, m_back_buffer, &logical_size, &new_size);
}

void Graphics::renderUpdate() 
{
	renderBackBufferToWindow();
	SDL_RenderPresent(m_renderer);
}

void Graphics::cleanup()
{
	SDL_DestroyWindow(m_window);
	m_window = NULL;
}