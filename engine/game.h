//#pragma once // Visual Studio's way of including header only once
#ifndef _GAME_H_ // Standard way of including header only once
#define _GAME_H_
#include "graphics.h"
#include "sceneHandler.h"
#include "fps.h"
#include "audioHandler.h"

class Game
{
private:
	Graphics *graphics;
	SceneHandler *sceneHandler;
	Fps *fps;
public:
	Game(const char* window_name = "Game", int game_width = 1920, int game_height = 1080);
	~Game();
	bool init();
	bool run();
	void cleanup();
	Graphics *getGraphics();
	SceneHandler *getSceneHandler();
};

#endif //_GAME_H_
