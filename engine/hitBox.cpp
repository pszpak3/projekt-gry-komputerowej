#include "hitBox.h"

HitBox::HitBox() {}

HitBox::HitBox(int x, int y, int w, int h) : m_x(x), m_y(y), m_w(w), m_h(h) {}

void HitBox::set(int x, int y, int w, int h)
{
	m_x = x;
	m_y = y; 
	m_w = w;
	m_h = h;
}

int HitBox::getX()
{
	return m_x;
}

int HitBox::getY()
{
	return m_y;
}

int HitBox::getW()
{
	return m_w;
}

int HitBox::getH()
{
	return m_h;
}
