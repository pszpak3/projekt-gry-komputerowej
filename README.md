## Silnik do tworzenia dwuwymiarowych gier komputerowych #

Repozytorium zawiera kod silnika i przykładowej gry z niego korzystającej.
Skompilowaną grę można pobrać pod [tym](https://drive.google.com/file/d/119_IWEFTjZfXJGmlmt4qSHYrGPW3jBtr) adresem.

### Jak utworzyć własną grę: #

1. Utwórz nowy projekt w dowolnym środowisku deweloperskim.
2. Pobierz i dodaj do projektu następujące biblioteki:
* Simple DirectMedia Layer 2 (SDL2),
* SDL Mixer,
* SDL Image,
* SDL TTF.
3. Pobierz z repozytorium folder *engine* i dodaj go do ścieżki projektu.
4. Dodaj wszystkie pliki z folderu *engine* do projektu.
5. Upewnij się, że Twój kompilator wspiera C++ w wersji 17.
6. Następnie postępuj zgodnie z poniższym przykładem:

##### Główny plik programu, czyli main.cpp powinien wyglądać następująco: #

```c++
#include "game.h"

int main(int argc, char **args)
{
	Game *game = new Game();
 
	game->init();
	//tutaj inicjalizujemy nasze obiekty i dodajemy je do scen
	game->run();
	game->cleanup();
 
	return 0;
}
```

##### Dodawanie obiektów do sceny jest bardzo intuicyjne i proste. #
##### Na początku deweloper tworzy klasę dziedziczącą po klasie Object #
```c++
class Box : public Object
{
	int wytrzymalosc;
 
public:
	Box(Graphics* graphics, SceneHandler* sceneHandler)
	: Object(graphics, sceneHandler)
	{
		wytrzymalosc = 5;
	}
};
```

##### Następnie w funkcji main między init() a run() tworzymy nasz obiekt. #
##### Bierzemy wskaźniki na obiekt Graphics oraz sceneHandler z obiektu game. #
```c++
Graphics* graphics = game->getGraphics();
SceneHandler* sceneHandler = game->getSceneHandler();
```

##### Tworzymy obiekt i wstrzykujemy zależności. #
```c++
Box* box = new Box(graphics, sceneHandler);
```

##### Możemy w prosty sposób załadować teksturę do tego obiektu. #
```c++
box->setTexture("sprites/box.png");
```

##### Aby dodać obiekt do sceny, bierzemy scenę jaka nas interesuje i dodajemy do niej utworzony obiekt. #
```c++
sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(box);
```

##### Tak wygląda funkcja main() po dodaniu do niej obiektów. #
```c++
int main(int argc, char** args)
{
	Game* game = new Game();
 
	game->init();
 
	Graphics* graphics = game->getGraphics();
	SceneHandler* sceneHandler = game->getSceneHandler();
	Box* box = new Box(graphics, sceneHandler);
	box->setTexture("sprites/box.png");
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(box);
 
	game->run();
	game->cleanup();
 
	return 0;
}
```

##### Dla przejrzystości utwórzmy funkcję odpowiadającą za tworzenie obiektów. #
```c++
void initObjects(Game* game)
{
	Graphics* graphics = game->getGraphics();
	SceneHandler* sceneHandler = game->getSceneHandler();
	Box* box = new Box(graphics, sceneHandler);
	box->setTexture("sprites/box.png");
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(box);
}
```

##### Ostatecznie main.cpp wygląda następująco: #
```c++
#include "game.h"
 
class Box : public Object
{
	int wytrzymalosc;
public:
	Box(Graphics* graphics, SceneHandler* sceneHandler) : Object(graphics, sceneHandler)
	{
		wytrzymalosc = 5;
	}
 
};

void initObjects(Game* game)
{
	Graphics* graphics = game->getGraphics();
	SceneHandler* sceneHandler = game->getSceneHandler();
 
	Box* box = new Box(graphics, sceneHandler);
	box->setTexture("sprites/box.png");
	sceneHandler->getScene(SCENE_FIRST_MAP)->addObject(box);
}
 
int main(int argc, char** args)
{
	Game* game = new Game();
 
	game->init();
	initObjects(game);
	game->run();
	game->cleanup();
 
	return 0;
}
```

##### Jeśli chcemy dodać całą sekwencję animacji, tworzymy obiekt klasy TextureSpriteSheet, reprezentujący ilość klatek w pliku oraz ich wymiary. #
```c++
TextureSpriteSheet idle_tss(1, 5, 46, 60);
```

##### Ładujemy do tego obiektu plik z klatkami. #
```c++
graphics->loadTexture(&idle_tss, "sprites/idle_box.png");
```

##### Tworzymy obiekt animacji. #
```c++
Animation* idle_animation = new Animation(idle_tss, 0.1, STATE_IDLE);
```

##### Następnie dodajemy animację do naszego obiektu. #
```c++
box->addAnimation(idle_animation);
```